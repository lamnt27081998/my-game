﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BoardManager : MonoBehaviour {

    public static BoardManager Instance { get; set; }
    private bool[,] allowedMoves { get; set; }

    public ChessPiece[,] ChessPieces { get; set; }
    private ChessPiece selectedChessPiece;

    private const float TITLE_SIZE = 1.0f;
    private const float TITLE_OFFSET = 0.5f;

    private int selectionX = -1;
    private int selectionY = -1;

    private int countW = 8;
    private int countB = 8;

    public List<GameObject> chessPiecesPrefabs;
    private List<GameObject> activeChessPieces;

    public bool isWhiteTurn = true;

    public void Start()
    {
        Instance = this;
        SpawnAllChessPieces();
    }

    private void Update()
    {
        UpdateSelection();
        DrawBoard();

        if (Input.GetMouseButtonDown(0))
        {
            if(selectionX >= 0 && selectionY >= 0)
            {
                if(selectedChessPiece == null)
                {
                    SelectChessPiece(selectionX, selectionY);
                }
                else
                {
                    MoveChessPiece(selectionX, selectionY);
                }
            }
        }
    }

    private void SelectChessPiece(int x, int y)
    {
        if (ChessPieces[x, y] == null)
            return;
        if (ChessPieces[x, y].isWhite != isWhiteTurn)
            return;

        bool hasAtLeastOneMove = false;
        allowedMoves = ChessPieces[x, y].PossibleMove();

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (allowedMoves[i, j])
                {
                    hasAtLeastOneMove = true;
                }
            }
        }

        if (!hasAtLeastOneMove)
            return;

        selectedChessPiece = ChessPieces[x, y];
        HighLight.Instance.HighlightAlowedMoves(allowedMoves);
    }

    private void MoveChessPiece(int x, int y)
    {
        int tempC = activeChessPieces.Count;
        if (allowedMoves[x, y])
        {   //ganh
            //check T-D
            if (y != 0 && y != 4)
            {
                if (ChessPieces[x, y + 1] != null && ChessPieces[x, y - 1] != null)
                {
                    if (ChessPieces[x, y + 1].isWhite != isWhiteTurn && ChessPieces[x, y - 1].isWhite != isWhiteTurn)
                    {
                        ganhRule(ChessPieces[x, y + 1], ChessPieces[x, y - 1]);
                    }
                }
            }

            //check L-R
            if(x != 0 && x != 4)
            {
                if (ChessPieces[x - 1, y] != null && ChessPieces[x + 1, y] != null)
                {
                    if (ChessPieces[x - 1, y].isWhite != isWhiteTurn && ChessPieces[x + 1, y].isWhite != isWhiteTurn)
                    {
                        ganhRule(ChessPieces[x - 1, y], ChessPieces[x + 1, y]);
                    }
                }
            }

            //check TL-DR
            if (x == 1 || x == 2 || x == 3)
            {
                if (y == 1 || y == 2 || y == 3)
                {
                    if (ChessPieces[x - 1, y + 1] != null && ChessPieces[x + 1, y - 1] != null)
                    {
                        if (ChessPieces[x - 1, y + 1].isWhite != isWhiteTurn && ChessPieces[x + 1, y - 1].isWhite != isWhiteTurn)
                        {
                            ganhRule(ChessPieces[x - 1, y + 1], ChessPieces[x + 1, y - 1]);
                        }
                    }
                }
            }

            //check DL-TR
            if (x == 1 || x == 2 || x == 3)
            {
                if (y == 1 || y == 2 || y == 3)
                {
                    if (ChessPieces[x - 1, y - 1] != null && ChessPieces[x + 1, y + 1] != null)
                    {
                        if (ChessPieces[x - 1, y - 1].isWhite != isWhiteTurn && ChessPieces[x + 1, y + 1].isWhite != isWhiteTurn)
                        {
                            ganhRule(ChessPieces[x - 1, y - 1], ChessPieces[x + 1, y + 1]);
                        }
                    }
                }
            }

            //chem
            if (y <= 2)
            {
                //check T
                if (ChessPieces[x, y + 1] != null && ChessPieces[x, y + 2] != null)
                {
                    if (ChessPieces[x, y + 1].isWhite != isWhiteTurn && ChessPieces[x, y + 2].isWhite == isWhiteTurn)
                    {
                        chemRule(ChessPieces[x, y + 1]);
                    }
                }
                //check TR
                if(x <= 2)
                {
                    if (ChessPieces[x + 1, y + 1] != null && ChessPieces[x + 2, y + 2] != null)
                    {
                        if (ChessPieces[x + 1, y + 1].isWhite != isWhiteTurn && ChessPieces[x + 2, y + 2].isWhite == isWhiteTurn)
                        {
                            chemRule(ChessPieces[x + 1, y + 1]);
                        }
                    }
                }

                //check TL
                if (x >= 2)
                {
                    if (ChessPieces[x - 1, y + 1] != null && ChessPieces[x - 2, y + 2] != null)
                    {
                        if (ChessPieces[x - 1, y + 1].isWhite != isWhiteTurn && ChessPieces[x - 2, y + 2].isWhite == isWhiteTurn)
                        {
                            chemRule(ChessPieces[x - 1, y + 1]);
                        }
                    }
                }
            }

            //check R
            if (x <= 2)
            {
                if (ChessPieces[x + 1, y] != null && ChessPieces[x + 2, y] != null)
                {
                    if (ChessPieces[x + 1, y].isWhite != isWhiteTurn && ChessPieces[x + 2, y].isWhite == isWhiteTurn)
                    {
                        chemRule(ChessPieces[x + 1, y]);
                    }
                }
            }

            //check L
            if (x >= 2)
            {
                if (ChessPieces[x - 1, y] != null && ChessPieces[x - 2, y] != null)
                {
                    if (ChessPieces[x - 1, y].isWhite != isWhiteTurn && ChessPieces[x - 2, y].isWhite == isWhiteTurn)
                    {
                        chemRule(ChessPieces[x - 1, y]);
                    }
                }
            }

            if (y >= 2)
            {
                //check DR
                if (x <= 2)
                {
                    if (ChessPieces[x + 1, y - 1] != null && ChessPieces[x + 2, y - 2] != null)
                    {
                        if (ChessPieces[x + 1, y - 1].isWhite != isWhiteTurn && ChessPieces[x + 2, y - 2].isWhite == isWhiteTurn)
                        {
                            chemRule(ChessPieces[x + 1, y - 1]);
                        }
                    }
                }

                //check D
                if (ChessPieces[x, y - 1] != null && ChessPieces[x, y - 2] != null)
                {
                    if (ChessPieces[x, y - 1].isWhite != isWhiteTurn && ChessPieces[x, y - 2].isWhite == isWhiteTurn)
                    {
                        chemRule(ChessPieces[x, y - 1]);
                    }
                }

                //check DL
                if (x >= 2)
                {
                    if (ChessPieces[x - 1, y - 1] != null && ChessPieces[x - 2, y - 2] != null)
                    {
                        if (ChessPieces[x - 1, y - 1].isWhite != isWhiteTurn && ChessPieces[x - 2, y - 2].isWhite == isWhiteTurn)
                        {
                            chemRule(ChessPieces[x - 1, y - 1]);
                        }
                    }
                }
            }
            if (isWhiteTurn)
            {
                countB -= (tempC - activeChessPieces.Count); 
            }
            else
            {
                countW -= (tempC - activeChessPieces.Count);
            }
            if (countW == 0 || countB == 0)
            {
                EndGame();
                return;
            }
            ChessPieces[selectedChessPiece.CurrentX, selectedChessPiece.CurrentY] = null;
            selectedChessPiece.transform.position = GetTileCenter(x, y);
            selectedChessPiece.SetPosition(x, y);
            ChessPieces[x, y] = selectedChessPiece;
            isWhiteTurn = !isWhiteTurn;
        }
        HighLight.Instance.HideHighlight();
        selectedChessPiece = null;
    }

    private void UpdateSelection()
    {
        if (!Camera.main)
            return;
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 15.0f, LayerMask.GetMask("ChessPlane")))
        {
            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }
    }

    private void SpawnChessPieces(int index, int x, int y)
    {
        GameObject go = Instantiate(chessPiecesPrefabs[index], GetTileCenter(x, y), Quaternion.identity) as GameObject;
        go.transform.SetParent(transform);
        ChessPieces[x, y] = go.GetComponent<ChessPiece>();
        ChessPieces[x, y].SetPosition(x, y);
        activeChessPieces.Add(go);

    }

    private void SpawnAllChessPieces()
    {
        activeChessPieces = new List<GameObject>();
        ChessPieces = new ChessPiece[8, 8];

        // spawn white pieces

        SpawnChessPieces(1, 0, 2);

        SpawnChessPieces(1, 0, 1);

        for (int i = 0; i <= 4; i++)
        {
            SpawnChessPieces(1, i, 0);
        }

        SpawnChessPieces(1, 4, 1);

        // spawn black pieces

        SpawnChessPieces(0, 4, 3);

        SpawnChessPieces(0, 4, 2);

        for (int i = 0; i <= 4; i++)
        {
            SpawnChessPieces(0, i, 4);
        }

        SpawnChessPieces(0, 0, 3);
    }

    private Vector3 GetTileCenter(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += x + TITLE_OFFSET;
        origin.y = 0.1f;
        origin.z += y + TITLE_OFFSET;
        return origin;
    }

    private void DrawBoard()
    {

        Vector3 lineWidth2 = Vector3.right * 4;
        Vector3 lineHeight2 = Vector3.forward * 4;

        Vector3 offset = new Vector3(0.5f, 0, 0.5f);


        for (int i = 0; i <= 4; i++)
        {
            Vector3 start = (Vector3.forward * i) + offset;

            Debug.DrawLine(start, start + lineWidth2);
            for (int j = 0; j <= 4; j++)
            {
                start = (Vector3.right * j) + offset;
                Debug.DrawLine(start, start + lineHeight2);
            }
        }

        Debug.DrawLine(Vector3.zero + offset, lineHeight2 + lineWidth2 + offset);
        Debug.DrawLine(lineHeight2 + offset, lineWidth2 + offset);

        Debug.DrawLine(Vector3.forward * 2 + offset, lineHeight2 + offset + (Vector3.right * 2));
        Debug.DrawLine(lineHeight2 + offset + (Vector3.right * 2), lineWidth2 + offset + (Vector3.forward * 2));
        Debug.DrawLine(lineWidth2 + offset + (Vector3.forward * 2), (Vector3.right * 2) + offset);
        Debug.DrawLine((Vector3.right * 2) + offset, (Vector3.forward * 2) + offset);

    }

    private void ganhRule(ChessPiece c, ChessPiece c2)
    {
        activeChessPieces.Remove(c.gameObject);
        activeChessPieces.Remove(c2.gameObject);
        Destroy(c.gameObject);
        Destroy(c2.gameObject);
    }

    private void chemRule(ChessPiece c)
    {
        activeChessPieces.Remove(c.gameObject);
        Destroy(c.gameObject);
    }

    private void EndGame()
    {
        if (isWhiteTurn)
            Debug.Log("White team wins!");
        else
            Debug.Log("Black team wins!");

        foreach(GameObject go in activeChessPieces)
        {
            Destroy(go);
        }
        isWhiteTurn = true;
        HighLight.Instance.HideHighlight();
        SpawnAllChessPieces();
        countB = 8;
        countW = 8;
    }
}
